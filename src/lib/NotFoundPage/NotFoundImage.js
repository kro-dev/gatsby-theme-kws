import React from 'react'
import { Image } from 'lib'
import { graphql, useStaticQuery } from 'gatsby'

const NotFoundImage = ({ children }) => {
  const data = useStaticQuery(graphql`
    query NotFoundImageQuery {
      image: file(
        sourceInstanceName: { eq: "theme-assets" }
        name: { eq: "bernard" }
      ) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)
  // return <>asd</>
  return <Image fluid={data.logo.childImageSharp.fluid} />
}

export default NotFoundImage
