import dot from './dot'
import arrowLeft from './arrowLeft'
import arrowRight from './arrowRight'

export default {
  dot,
  arrowLeft,
  arrowRight
}
