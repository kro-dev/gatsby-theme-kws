import React from 'react'
import { Input as CInput } from '@chakra-ui/core'
/** @jsx jsx */
import { jsx } from 'theme-ui'

export const styles = {
  default: {},
  lg: {
    fontSize: 'lg',
    px: 4,
    minHeight: 16,
    lineHeight: '3rem',
    rounded: 'md',
  },
  md: {
    fontSize: 'md',
    px: 4,
    minHeight: 12,
    lineHeight: '2.5rem',
    rounded: 'md',
  },
  sm: {
    fontSize: 'sm',
    px: 3,
    minHeight: 10,
    lineHeight: '2rem',
    rounded: 'sm',
  },
}

const SizeThemedInput = React.forwardRef(({ size, ...props }, ref) => (
  <CInput sx={{ variant: `input.${size}` }} ref={ref} {...props} />
))
const Input = React.forwardRef((props, ref) => (
  <SizeThemedInput sx={{ variant: 'input.default' }} ref={ref} {...props} />
))

Input.defaultProps = {
  size: 'md',
  rounded: 'default',
  borderWidth: 2,
  focusBorderColor: 'primary.500',
}

const Textarea = React.forwardRef((props, ref) => {
  return (
    <Input
      py="8px"
      minHeight="80px"
      lineHeight="short"
      ref={ref}
      as="textarea"
      {...props}
    />
  )
})
Input.Textarea = Textarea
export default Input
