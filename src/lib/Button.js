import React from 'react'
/** @jsx jsx */
import { jsx, useThemeUI } from 'theme-ui'
import { Button as ChakraButton } from '@chakra-ui/core'
import { motion } from 'framer-motion'
// import { useHover } from 'hooks'//TODO:remove from lib
import { useHover } from 'react-use-gesture'
import { useMotionValue, useSpring } from 'framer-motion'

import { get } from 'theme/utils'

ChakraButton.defaultProps = { ...ChakraButton.defaultProps, rounded: 'default' }

export const styles = {
  default: {
    // textTransform: 'uppercase',
  },
  outline: {
    default: { border: '2px' },
    hover: { border: '1px' },
  },
  sizes: {
    xl: {
      height: 16,
      minWidth: 14,
      fontSize: ['lg', 'xl', null, '2xl'],
      px: 8,
    },
    lg: {
      height: 12,
      minWidth: 12,
      fontSize: ['md', 'lg', null, 'xl'],
      px: 6,
    },
    md: {
      height: 10,
      minWidth: 10,
      fontSize: ['sm', 'md', null, 'lg'],
      px: 4,
    },
    sm: {
      height: 8,
      minWidth: 8,
      fontSize: ['xs', 'sm', null, 'md'],
      px: 3,
    },
    xs: {
      height: 6,
      minWidth: 6,
      fontSize: ['2xs', 'xs', null, 'sm'],
      px: 2,
    },
  },
}

//temp wrapper until we get multiple variatns in theme-ui
//https://github.com/system-ui/theme-ui/issues/403
const SizedButton = ({ size = 'md', ...props }) => (
  <ChakraButton
    size={size}
    {...props}
    sx={{ variant: `button.sizes.${size}` }}
  />
)

const Button = props => (
  <SizedButton {...props} sx={{ variant: 'button.default' }} />
)
// Button.defaultProps.variantColor = 'secondary'

//TODO:set default size to 'button.default', and set 'default' key in theme
//also do border theme prop and key to default

const LinkButton = props => <Button {...props} variant="link" />
LinkButton.displayName = 'Button.Link'
Button.Link = LinkButton

const GhostButton = props => <Button {...props} variant="ghost" />
GhostButton.displayName = 'Button.Ghost'
Button.Ghost = GhostButton

const OutlineButton = ({
  transition,
  children,
  hover = { bg: 'primary.500', color: 'white' },
  ...rest
}) => {
  const { theme } = useThemeUI()
  const x = useSpring('130%', { damping: 100, stiffness: 200 })
  const opacity = useSpring(0, { damping: 100, stiffness: 200 })
  // const color = useSpring(defaultColor, { damping: 100, stiffness: 200 })
  // const { colors } = theme
  // console.log(colors, 'colors')
  const bind = useHover(({ hovering }) => {
    x.set(hovering ? '0%' : '130%')
    opacity.set(hovering ? 1 : 0)
    // color.set(hovering ? hoverColor : defaultColor)
  }) //TODO:we could potentially refactor to use framer-motion onHoverStart? this rerenders every hover

  const hoverColor = React.useMemo(() => get(theme, hover.color) || 'black', [
    hover, //TODO: do we need to react to theme object as well?
  ])
  const tempsx = {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: '100%',
    height: '100%',
    color: hoverColor,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 3,
  }
  const defaultColor = get(theme, rest.color || 'white')

  //TODO: change from animating color (causes a repaint)

  //TODO: create reusable framer spring configs (much like react-spring.config.[slow, wobbly, etc])
  return (
    <Button
      {...bind()}
      _hover={{ bg: 'transparent' }}
      as="a"
      sx={{
        overflow: 'hidden',
        position: 'relative',
        cursor: 'pointer',
        variant: 'button.outline.default',
      }}
      overflow="hidden"
      {...rest}
      variant="outline"
    >
      <motion.div
        sx={{ zIndex: 3 }}
        // style={{ color }}
        // animate={{ color: isHovering ? hoverColor : defaultColor }}
      >
        {children}
      </motion.div>
      <motion.div sx={tempsx} style={{ opacity }}>
        {children}
      </motion.div>
      <motion.div
        // variants={{ default: { x: '130%' }, hover: { x: '0%' } }}
        // initial="default"
        // animate={isHovering ? 'hover' : 'default'}
        transition={{ damping: 10, stiffness: 200 }}
        sx={{
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          width: '100%',
          height: '100%',
          zIndex: 2,
          ...hover,
        }}
        style={{ x }}
      />
    </Button>
  )
}

//https://github.com/vigetlabs/react-ink or muicss / material-ui for ripple on click

OutlineButton.defaultProps = { transition: 'default' }
OutlineButton.displayName = 'Button.Outline'

OutlineButton.whyDidYouRender = true
Button.Outline = OutlineButton

export default Button

//TODO: other button variants
//TODO:pressed state anim
