import React, { useState } from 'react'
/** @jsx jsx */
import { jsx, useThemeUI } from 'theme-ui'
import Carousel, { Modal, ModalGateway } from 'react-images'
import FsLightbox from 'fslightbox-react'

import { Box, Flex, Image, Masonry, Text } from 'lib' //Modal
import { useToggle } from 'hooks'
import { Global } from '@emotion/core'
export const styles = {
  columns: [2, 4, 4, 5, 6],
  thumbnail: {
    borderRadius: 'none',
    ':hover': { filter: 'brightness(87.5%)' },
  },
}

const Gallery = ({ images, columns }) => {
  const { theme } = useThemeUI()

  const [isOpen, toggle] = useToggle()
  //TODO:finish when chakra fixes scrollbar flicker
  //https://github.com/chakra-ui/chakra-ui/issues/129
  const [modalCurrentIndex, setModalCurrentIndex] = useState(0)

  const openModal = imageIndex => {
    setModalCurrentIndex(imageIndex)
    toggle()
  }
  //create a new shaped array for react-images
  const lbImages = images.map(({ id, image }) => ({
    ...image,
    caption: 'test',
  }))
  console.log('lbImages', lbImages)
  //end temp solution
  return (
    <>
      <Masonry columns={columns || theme.gallery.columns}>
        {/* {Object.entries(images).map(entry => {
          const id = entry[0]
          const { thumb, image } = entry[1]

          return (
            <Masonry.Item aspectRatio={thumb.aspectRatio} key={id}>
              <a
                key={id}
                href={thumb.originalImg}
                onClick={e => {
                  e.preventDefault()
                  // toggle()
                  openModal(id)
                }}
              >
                <Image
                  sx={{ variant: 'gallery.thumbnail' }}
                  fluid={thumb}
                  // title={image.caption}
                  width="100%"
                  height="100%"
                />
              </a>
            </Masonry.Item>
          )
        })} */}
        {images.map(({ id, thumb }, i) => (
          <Masonry.Item aspectRatio={thumb.aspectRatio} key={id}>
            <a
              key={id}
              href={thumb.originalImg}
              onClick={e => {
                e.preventDefault()
                // toggle()
                openModal(i)
              }}
            >
              <Image
                sx={{ variant: 'gallery.thumbnail' }}
                fluid={thumb}
                // title={image.caption}
                width="100%"
                height="100%"
              />
            </a>
          </Masonry.Item>
        ))}
      </Masonry>
      <FsLightbox
        toggler={isOpen}
        sources_={images.map(img => img.image.src)}
        customSources={images.map(({ image, alt }) => {
          return (
            <Flex
              width="90vw"
              height="90vh"
              vAlignContent="center"
              hAlignContent="center"
            >
              <Image
                width="100%"
                fluid={image}
                style={{ maxWidth: image.presentationWidth }}
                alt={alt}
              />
              {alt && (
                <Text
                  position="absolute"
                  bottom={0}
                  color="white"
                  sx={{ transform: 'translateY(100%)' }}
                >
                  {alt}
                </Text>
              )}
            </Flex>
          )
        })}
        sourceIndex={modalCurrentIndex}
      />
      {/* <Modal isOpen={isOpen} onClose={toggle}>
        <Modal.Overlay />
        <Modal.Content>
          <Modal.Header>Modal Title</Modal.Header>
          <Modal.CloseButton onClick={toggle} />
          <Modal.Body>test </Modal.Body>

          <Modal.Footer>fo</Modal.Footer>
        </Modal.Content>
      </Modal> */}
      <Global styles={{ '.react-images__view-image': { display: 'inline' } }} />
      {/* {ModalGateway && (
        <ModalGateway>
          {isOpen && (
            <Modal
              onClose={toggle}
              styles={{
                blanket: (base, state) => ({ ...base, zIndex: 10100 }),
                positioner: (base, state) => ({ ...base, zIndex: 10110 }),
                dialog: (base, state) => ({ ...base, zIndex: 10120 }),
              }}
            >
              <Carousel
                views_="{images.map(({ originalImg, caption }) => ({
                  source: originalImg,
                  caption,
                }))}"
                views={lbImages}
                currentIndex={modalCurrentIndex}
                // formatters={carouselFormatters}
                components={{ FooterCount_: () => null, View_: View }}
              />
            </Modal>
          )}
        </ModalGateway>
      )} */}
    </>
  )
}

const View = props => {
  // console.log(props, '*')
  return (
    <Flex>
      <span>Test</span>
      <Image height="100%" width="auto" fluid={props.data} />
    </Flex>
  )
}

//NOTE: For some reason, a few images were displaying with weird gaps/padding
//due to the aspect ratio reported from image-sharp query being incorrect.
//I have no idea why.  The fix is to open the file in windows photo editor
//and resave/overwrite
export default Gallery
