//https://march08.github.io/animated-burgers/
export { default as BurgerArrow } from './BurgerArrow'
export { default as BurgerRotate } from './BurgerRotate'
export { default as BurgerSlide } from './BurgerSlide'
export { default as BurgerSlip } from './BurgerSlip'
export { default as BurgerSqueeze } from './BurgerSqueeze'
