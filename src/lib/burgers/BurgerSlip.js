import React from 'react'
import Burger from '@animated-burgers/burger-slip'
import '@animated-burgers/burger-slip/dist/styles.css'

const BurgerSlip = props => <Burger {...props} />
export default BurgerSlip
