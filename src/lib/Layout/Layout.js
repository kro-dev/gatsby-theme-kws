import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Global } from '@emotion/core'
import { useSpring, config } from 'react-spring'
import { Hide } from 'lib'
// import { useLocation, useLayoutTransition } from 'hooks'
import { Footer, LayoutBase, Navbar, ModalNav, NavToggle } from './'
import { GoogleReCaptchaProvider } from 'react-google-recaptcha-v3'
import { useTheme } from '@chakra-ui/core'
import TransitionPortal from 'lib/Layout/TransitionContext/TransitionPortal' //TODO:clean up
/** @jsx jsx */
import { jsx } from 'theme-ui'

///backup props for vertical slide animation
// from = {{ opacity: 0, transform: 'translate3d(0,-40px,0)' }}
// to = {{ opacity: 1, transform: 'translate3d(0,0px,0)' }}
///
//TODO:make it easy to override transition portal to/from per project
const TransitionOverlay = props => {
  return (
    <TransitionPortal
      exit={{
        from: { transform: 'translate3d(100%,0,0)' },
        to: { transform: 'translate3d(0,0,0)' },
      }}
      enter={{
        from: { transform: 'translate3d(0,0,0)' },
        to: { transform: 'translate3d(-100%,0,0)' },
      }}
      routeChangeOnly
      sx={{
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        bg: 'primary.500',
        zIndex: 'transition-overlay',
      }}
    />
  )
}
//TODO: move to default props
const Layout = ({ children, isLayoutHidden = false }) => {
  const theme = useTheme()

  //TODO:conver to  component/provider
  const modalNavOnly = theme.config.modalNavOnly
  console.log(modalNavOnly, 'modalNavOnly')

  const [visible, setVisible] = useState(false) //TODO:update to useToggle
  const toggleNav = () => setVisible(!visible)

  const animation = useSpring({
    positive: visible ? 100 : 0,
    negative: visible ? 0 : 100,
    config: config.slow,
  })

  //TODO:handle wrapping modalnavonly or not cleaner rather than ternary
  //TODO:React.StrictMode
  return (
    <>
      {/* <CSSReset config={theme.config.chakraConfig} /> */}

      <Global
        styles={theme => ({
          body: {
            // MozOsxFontSmoothing: 'grayscale',
            // WebkitFontSmoothing: 'antialiased',
            // fontFamily: theme.fonts.body,
            //from animation:
            overflow: visible ? `hidden` : `visible`,
          },
          '.grecaptcha-badge': { display: 'none' },
        })}
      />
      <LayoutBase>
        {!isLayoutHidden && (
          <LayoutBase.Header>
            {modalNavOnly ? (
              <NavToggle isOpen={visible} onClick={toggleNav} />
            ) : (
              <Hide md lg xl>
                {/* <span>a</span> */}
                <NavToggle isOpen={visible} onClick={toggleNav} />
              </Hide>
            )}

            {!modalNavOnly && (
              <Hide xs sm>
                <Navbar />
              </Hide>
            )}

            <ModalNav
              visible={visible}
              animation={animation}
              toggle={toggleNav}
            />
          </LayoutBase.Header>
        )}

        <LayoutBase.Main
          todo="figure out what the animated style below is doing"
          overflow="hidden"
          style={{
            transform: animation.positive.interpolate(
              y => `translate3d(0, ${y}vh, 0)`
            ),
          }}
        >
          <TransitionOverlay />

          {/* <TransitionPortal
            symmetric={false}
            sx={{ padding: '20px' }}
            enter={{ background: 'red' }}
            rest={{ background: 'blue' }}
            exit={{ background: 'yellow' }}
          >
            123
          </TransitionPortal> */}
          {children}
        </LayoutBase.Main>
        {!isLayoutHidden && (
          <LayoutBase.Footer>
            <Footer />
          </LayoutBase.Footer>
        )}
      </LayoutBase>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

//TODO:need to have a option to disable recaptcha and not wrap with GoogleRecaptchaProvider
//TODO:should location be handled at the Layout Level, or just pass it down to the menus?
const EnhancedLayout = ({ children, ...rest }) => (
  <GoogleReCaptchaProvider reCaptchaKey={process.env.GATSBY_RECAPTCHA_SITE_KEY}>
    <Layout>{children}</Layout>
  </GoogleReCaptchaProvider>
)

// export default Layout
export default EnhancedLayout
