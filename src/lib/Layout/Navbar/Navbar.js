import React from 'react'
import { NavbarTemplate, NavLink } from './'
import { useSiteMetadata } from 'hooks'
import { Location } from '@reach/router'

const Header = props => {
  const { menuLinks } = useSiteMetadata()
  return (
    <Location>
      {({ location }) => (
        <NavbarTemplate location={location} {...props}>
          {menuLinks.map(({ name, to }) => (
            <NavLink to={to} key={`${name}:${to}`}>
              {name}
            </NavLink>
          ))}
        </NavbarTemplate>
      )}
    </Location>
  )
}
export default Header
