import React from 'react'
/** @jsx jsx */
import { jsx } from 'theme-ui'
import { Flex, Spacer } from 'lib'
import { Logo } from './'
// import { Link } from 'gatsby'
import TransitionLink from 'lib/Layout/TransitionContext/TransitionLink' //TODO:clean up

const MenuContainer = ({ children }) => (
  <Flex as="ul" height="100%" row hAlignContent="right" vAlignContent="stretch">
    {children}
  </Flex>
)

//TODO:seperatre out uppercase styling etc
const NavbarTemplate = ({ children, ...rest }) => {
  return (
    <Flex
      as="nav"
      row
      role="navigation"
      grow
      sx={{ variant: 'layout.navbar.container' }}
      {...rest}
    >
      <Flex grow row vAlignContent="center">
        <TransitionLink
          to="/"
          key="home"
          sx={{ variant: 'layout.navbar.logo' }}
        >
          <Logo />
        </TransitionLink>
        <Spacer />
        <MenuContainer>{children}</MenuContainer>
      </Flex>
    </Flex>
  )
}

export default NavbarTemplate
