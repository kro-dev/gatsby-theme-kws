import { styles as navbar } from './Navbar'
import { styles as modalNav } from './ModalNav'

export default {
  navbar,
  modalNav,
  header: {
    backgroundColor: 'transparent',
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 'layout-header',
    minHeight: [12, null, 100, null, 144],
  },
  main: {
    backgroundColor: 'white',
    position: 'relative',
    mt: [12, null, 100, null, 144],
  },
  footer: {
    backgroundColor: 'secondary.500',
    color: 'white',
    py: [2, null, null, 18],
    px: 2,
    zIndex: 'footer',
  },
}
