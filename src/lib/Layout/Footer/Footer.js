import React from 'react'
import { FooterTemplate } from './'
import { useSiteMetadata } from 'hooks'

const Footer = () => {
  const { title } = useSiteMetadata()
  return <FooterTemplate title={title} />
}
export default Footer
