import React from 'react'
import { TransitionProvider } from './'

const wrapRootElement = ({ element }) => (
  <TransitionProvider>{element}</TransitionProvider>
)

export default wrapRootElement
