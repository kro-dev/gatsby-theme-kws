//https://github.com/Paratron/hookrouter/blob/master/src/interceptor.js

import React from 'react'

let incrementalId = 1

const interceptors = []

export const interceptRoute = (previousRoute, nextRoute) => {
  if (!interceptors.length) {
    return nextRoute
  }

  return interceptors.reduceRight(
    (nextRoute, interceptor) =>
      nextRoute === previousRoute
        ? nextRoute
        : interceptor.handlerFunction(previousRoute, nextRoute),
    nextRoute
  )
}

const get = componentId =>
  interceptors.find(obj => obj.componentId === componentId) || null
const remove = componentId => {
  const index = interceptors.findIndex(obj => obj.componentId === componentId)
  if (index !== -1) {
    interceptors.splice(index, 1)
  }
}

export const useInterceptor = handlerFunction => {
  const [componentId] = React.useState(incrementalId++)

  let obj = get(componentId)

  if (!obj) {
    obj = {
      componentId,
      stop: () => remove(componentId),
      handlerFunction,
    }

    interceptors.unshift(obj)
  }

  React.useEffect(() => () => obj.stop(), [])

  return obj.stop
}

/**
 * This is a controlled version of the interceptor which cancels any navigation intent
 * and hands control over it to your calling component.
 *
 * `interceptedPath` is initially `null` and will be set to the target path upon navigation.
 * `confirmNavigation` is the callback to be called to stop the interception and navigate to the last path.
 * `resetPath` is a callback that resets `interceptedPath` back to `null`.
 *
 * @returns {Array} [interceptedPath, confirmNavigation, resetPath]
 */
export const useControlledInterceptor = () => {
  const [interceptedPath, setInterceptedPath] = React.useState(null)

  const interceptorFunction = React.useMemo(
    () => (currentPath, nextPath) => {
      console.log(currentPath, nextPath)

      setInterceptedPath(nextPath)
      return currentPath
    },
    [setInterceptedPath]
  )

  const stopInterception = useInterceptor(interceptorFunction)

  const confirmNavigation = React.useMemo(
    () => () => {
      stopInterception()
      // navigate(interceptedPath);
      //console.log('NAVIGATING TO: ' + interceptedPath)
    },
    [stopInterception, interceptedPath]
  )

  const resetPath = React.useMemo(() => () => setInterceptedPath(null), [
    setInterceptedPath,
  ])

  return [interceptedPath, confirmNavigation, resetPath, stopInterception]
}
