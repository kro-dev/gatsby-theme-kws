import React from 'react'
import { animated, useSpring } from 'react-spring'
import { useLayoutTransition } from 'hooks' //TODO:name useRouteTranstion
import { useInterceptor, useControlledInterceptor } from './interceptor' //TODO:provide in /hooks
import { useId } from '@reach/auto-id'
//TODO:consider renaming onBeforeAnimateOut and such to onBeforeLeave, and then abstracting out a layer for use without animation
//such as prompting before route change etc

// const interceptFunction = (currentPath, nextPath) => {
//   return new Promise(function(resolve, reject) {
//     let confirmed = confirm('Do you want to leave?')

//     return confirmed ? resolve(nextPath) : reject(currentPath)
//   })
// }

const TransitionPortal = ({
  symmetric = false,
  routeChangeOnly = false,
  enter,
  rest = false,
  exit,
  onBeforeAnimateOut,
  onBeforeNavigate,
  onAfterNavigate,
  onAfterAnimateIn,
  ...props
}) => {
  //   console.log(props)
  const [transitionState, dispatch] = useLayoutTransition()
  // const stopInterceptor = useInterceptor(interceptFunction)

  // console.log(transitionState, 'transitionState')

  //change leave to exit
  //memo
  //   enter = css(enter)
  //   rest = css(rest)
  // console.log(css(transitionMap)(useTheme), '*')

  switch (transitionState.status) {
    case 'before-animate-out':
      onBeforeAnimateOut()
    case 'before-navigate':
      onBeforeNavigate()
    case 'after-navigate':
      onAfterNavigate()
    case 'after-animate-in':
      onAfterAnimateIn()
  }
  //
  let transitionMap
  if (rest) {
    transitionMap = {
      initial: { to: rest },
      enter: { from: enter, to: rest },
      exit: { from: rest, to: symmetric ? enter : exit },
    }
  } else {
    //from and to keys inside enter/leave
    transitionMap = {
      initial: { to: enter.to },
      enter,
      exit,
    }
  }
  if (!routeChangeOnly) {
    transitionMap.initial.from = enter
  }
  // console.log(transitionMap, 'transitionMap')

  return React.useMemo(() => {
    return (
      <TPInner
        dispatch={dispatch}
        transitionState={transitionState}
        {...transitionMap[transitionState.spring]}
        {...props}
      />
    )
  }, [transitionState.spring, props.children])
}
//support themed anim values
//support starting from rest vs starting from leave and having to enter
//support two step (anim out, rest, anim in)
//as well as single (from, to)
const TPInner = ({
  children,
  config,
  from,
  to,
  toggle,
  Component = animated.div,
  dispatch,
  transitionState,
  onStart,
  onRest_,
  onEnd,
  ...props
}) => {
  console.log(transitionState.status, 'status render')
  console.log(from, 'from')
  console.log(to, 'to')

  // const stopInterceptor = useInterceptor(interceptFunction)
  const [
    nextPath,
    confirmNavigation,
    resetPath,
    stopInterception,
  ] = useControlledInterceptor()

  const id = useId()
  //
  const animatedProps = useSpring({
    from,
    to,
    // onRest: e => {
    //   //   navigate(transitionState.to)
    //   interceptResolve.resolve()
    //   // dispatch({ type: 'animation-complete' }) //this fires multiple times. can only use this way if singleton transition-overlay
    //   //   dispatch({ type: 'enter' })
    // },
    onRest: e => {
      dispatch({ type: 'animation-complete', payload: { id } })

      if (transitionState.to) {
        confirmNavigation(transitionState.to)
      }
    },
    config, //
    onStart: e => {
      dispatch({ type: 'animation-start', payload: { id } })
    },
  }) //TODO: add will change
  return (
    <Component style={animatedProps} {...props}>
      {children}
    </Component>
  )
}

export default TransitionPortal
