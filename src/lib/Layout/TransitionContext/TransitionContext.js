import React, {
  createContext,
  useReducer,
  useCallback,
  useEffect,
  useState,
} from 'react'
import { interceptRoute } from './interceptor'
import { navigate as gatsbyNavigate } from 'gatsby'
import { useAnimation, motion } from 'framer-motion'
import { createContainer } from 'react-tracked'

const INITIAL_STATE = {
  replace: false,
  to: undefined,
  linkState: null,
  variant: undefined, //which variant is being ran enter/exit
  inFlight: false, //if the animations are currently running
}
//TODO:consider just using useRef instead of useTracked since none of the state is ever needed to cause a rerender in
//view layer

const useValue = ({ controls }) => {
  // console.log(initialState, 'transitionState-initialState')
  const [state, setState] = useState({ ...INITIAL_STATE, controls })
  const linkRef = React.useRef()
  // const transitionOut = async () => {
  //   console.log('transitionState-start transition out')
  //   // setState(state => ({ ...state, status: 'exiting', canNavigate: false }))
  //   console.log('transitionState-before call exit', state.controls)
  //   state.controls.start('exit')
  //   // setState(state => ({ ...state, status: 'exited', canNavigate: true }))
  //   console.log('transitionState-end transition out')
  // }
  const triggerAnimation = async variant => {
    console.log('transitionState-triggerAnim-START', variant)
    setState(state => ({ ...state, inFlight: true, variant }))
    await state.controls.start(variant)
    console.log('transitionState-triggerAnim-END', variant)
    setState(state => ({ ...state, inFlight: false }))
  }
  const triggerTransitionOut = async () => {
    await triggerAnimation('exit')
    return
  }
  const triggerTransitionIn = async () => {
    await triggerAnimation('enter')
    return
  }

  const setNavigationTarget = options => (linkRef.current = options)

  // const setNavigationTarget = options =>
  //   setState(state => ({ ...state, ...options }))
  const navigate = async options => {
    //if currently animating out (we have a target), then replace target
    setNavigationTarget(options)

    if (!state.inFlight) {
      //if not currently animating out, set target and trigger animate-out
      await triggerTransitionOut()
      console.log('transitionstate-routing to ', linkRef.current)
      gatsbyNavigate(linkRef.current.to, {
        replace: linkRef.current.replace,
        state: linkRef.current.linkState,
      })
    }
  }

  return [
    state,
    { triggerTransitionOut, triggerTransitionIn, navigate, triggerAnimation },
  ]
}

//TODO:change name from Transition -- useTransition conflicts with React useTransition
export const {
  Provider: InternalProvider,
  useTracked: useTransition,
  useTrackedState: useTransitionState,
  useUpdate: useTransitionActions,
} = createContainer(useValue)

// export const navigate = (to, { state, replace }, dispatch) => {
//   const currentPath = location.pathname

//   to = interceptRoute(currentPath, to)

//   if (!to || to === currentPath) {
//     return
//   }

//   // dispatch({
//   //   type: 'navigate-to',
//   //   payload: { to, replace, linkState: state },
//   // })
//   console.log('*******navigated to new page****')
// }

//export const LayoutContext = React.createContext(defaultContextValue)
export const TransitionContext = createContext() //TODO:standardize name layout/transition + cleanup

//TODO:consider trying concurrent mode to to make animations smoother
export const TransitionProvider = ({ children, delay = 600, ...props }) => {
  const controls = useAnimation()

  return (
    <InternalProvider controls={controls}>
      <motion.div
        // variants={{ enter: { opacity: 1 }, exit: { opacity: 0.9 } }}
        animate={controls}
        style={{ height: '100%', width: '100%' }}
      >
        {children}
      </motion.div>
    </InternalProvider>
  )
}

TransitionProvider.displayName = 'TransitionProvider'
TransitionProvider.whyDidYouRender = true

export default TransitionContext
