import React from 'react'
/** @jsx jsx */
import { jsx } from 'theme-ui'
import { Box } from 'lib'
import { BurgerSqueeze } from 'lib/burgers'

export const styles = {
  container: {
    position: 'fixed',
    right: 0,
    top: 0,
    zIndex: 3000,
    bg: 'transparent',
    padding: 1,
  },
  burger: {
    fontSize: ['0.75em', '1.06em', '1.1em'],

    '.burger-lines, .burger-lines:before, .burger-lines:after': {
      backgroundColor: 'white',
    },
  },
}

//TODO: handle left/right

const Container = ({ isOpen, ...rest }) => {
  return (
    <Box
      sx={{
        variant: 'layout.modalNav.toggle.container',
      }}
      {...rest}
    />
  )
}

const Burger = BurgerSqueeze

const NavToggle = ({ isOpen, direction, Component, type, ...props }) => {
  return (
    <Container {...props}>
      <Burger
        isOpen={isOpen}
        direction={direction}
        Component={Component}
        type={type}
        sx={{ variant: 'layout.modalNav.toggle.burger' }}
      />
    </Container>
  )
}

export default NavToggle
