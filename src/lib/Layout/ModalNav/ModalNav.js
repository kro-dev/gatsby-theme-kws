import React from 'react'
/** @jsx jsx */
import { jsx } from 'theme-ui'
import { useSpring, animated, config } from 'react-spring'
import { useSiteMetadata } from 'hooks'
import { Flex } from 'lib'
import { Logo, NavLink } from './'

//ModalNav //menu items, animation
//container //logo, menu
//wrapper //wrapper, menu, menuitems

const ModalNav = ({ animation, visible, toggle }) => {
  const { menuLinks } = useSiteMetadata()

  const subAnimation = useSpring({
    transform: `translate3d(0, ${visible ? 0 : -50}vh, 0)`,
    opacity: visible ? 1 : 0,
    config: config.slow,
  })

  return (
    <ModalNav.Container
      style={{
        transform: animation.negative.interpolate(
          y => `translate3d(0, ${-y}vh, 0)`
        ),
      }}
    >
      <ModalNav.Inner style={subAnimation}>
        {menuLinks.map(({ name, to }) => (
          <NavLink to={to} key={`${name}:${to}`} toggle={toggle}>
            {name}
          </NavLink>
        ))}
      </ModalNav.Inner>
    </ModalNav.Container>
  )
}

const Container = props => (
  <Flex
    as={animated.nav}
    height="100vh"
    width="full"
    position="absolute"
    overflow="hidden"
    zIndex={2000}
    sx={{ variant: 'layout.modalNav.container' }}
    {...props}
  />
)
ModalNav.Container = Container

const Inner = ({ children, ...rest }) => (
  <>
    <Flex
      hAlignContent="center"
      vAlignContent="center"
      mt={4}
      basis="30%"
      sx={{ variant: 'layout.modalNav.menu' }}
    >
      <Flex sx={{ variant: 'layout.modalNav.logo' }}>
        <Logo />
      </Flex>
    </Flex>

    <ModalNav.Menu {...rest}>{children}</ModalNav.Menu>
  </>
)
ModalNav.Inner = Inner

const Menu = props => (
  <Flex
    as={animated.ul}
    mr={2}
    mb={2}
    {...props}
    grow
    vAlignContent="space-around"
    p={8}
  />
)
Menu.defaultProps = {
  hAlignContent: 'center',
}
ModalNav.Menu = Menu

export default ModalNav
