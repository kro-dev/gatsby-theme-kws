//https://github.com/hooroo/roo-ui/blob/alpha/src/components/Flex/Flex.js
import React, { forwardRef } from 'react'
import { style, colorStyle } from 'styled-system'
import styled from '@emotion/styled'
// import { Box } from 'lib'
import Box from 'lib/Box/Box'
// import { Flex } from '@chakra-ui/core'

const Flex = forwardRef(
  (
    {
      align,
      justify,
      wrap,
      direction,
      orientationLocked = false,
      reverseOrientationLocked = false,
      _landscape = {},
      ...rest
    },
    ref
  ) => {
    if (orientationLocked) {
      _landscape.flexDirection = 'row'
    }
    if (reverseOrientationLocked) {
      _landscape.flexDirection = 'column'
    }
    return (
      <Box
        ref={ref}
        display="flex"
        flexDirection={direction}
        alignItems={align}
        justifyContent={justify}
        flexWrap={wrap}
        _landscape={_landscape}
        {...rest}
      />
    )
  }
)

Flex.displayName = 'Flex'

const alignToFlex = align => {
  switch (align) {
    case 'top':
    case 'left':
      return 'flex-start'
    case 'center':
      return 'center'
    case 'bottom':
    case 'right':
      return 'flex-end'
    default:
      return align
  }
}

const getGrow = grow => {
  if (typeof grow === 'number') {
    return grow
  } else if (grow) {
    return 1
  }

  return 0 // default
}
//switch to flex-grow, flex-shrink, basis for readability
const getShrink = (shrink, basis) => {
  if (typeof shrink === 'number') {
    return shrink
  } else if (shrink) {
    return 1
  } else if (shrink === false) {
    return 0
  }

  if (basis && basis !== 'auto') {
    return 0
  }

  return 1 // default
}

const getBasis = basis => {
  if (typeof basis === 'number') {
    return `${basis}rem`
  }
  if (!basis) {
    return 'auto'
  }
  return basis
}
//TODO:consider removing in favor of Grid + gap or refactoring to use css and :first-child/ :last-child
const processChildrenForSpacing = (
  children,
  spacing,
  row,
  shouldWrapChildren
) => {
  //isRow
  return React.Children.map(children, (child, index) => {
    if (!React.isValidElement(child)) return
    let isLastChild = children.length === index + 1
    let spacingProps = row
      ? { mr: isLastChild ? null : spacing }
      : { mb: isLastChild ? null : spacing }

    let newChild = child
    if (child.type.shouldForwardSpacing) {
      const _children = processChildrenForSpacing(
        child.props.children,
        spacing,
        row,
        shouldWrapChildren
      )
      newChild = React.cloneElement(child, spacingProps, _children)
      console.log('clone', newChild)
    }
    // if (child.type.displayName === 'AnimateChildren') {
    //   console.log('clone', child)
    // }

    if (shouldWrapChildren) {
      return (
        <Box d="inline-block" {...spacingProps}>
          {newChild}
        </Box>
      )
    }
    return React.cloneElement(newChild, spacingProps)
  })
}

const FlexContainer = forwardRef(
  (
    {
      children,
      centered,
      row,
      flex,
      grow,
      shrink,
      basis,
      hAlignContent,
      vAlignContent,
      reverse,
      spacing,
      shouldWrapChildren,
      ...rest
    },
    ref
  ) => {
    // const flexCssVal = `${getGrow(grow)} ${getShrink(shrink, basis)} ${getBasis(
    //   basis
    // )}`
    // const justify = alignToFlex(col ? vAlignContent : hAlignContent)
    // const items = alignToFlex(col ? hAlignContent : vAlignContent)
    const justify = alignToFlex(row ? hAlignContent : vAlignContent)
    const align = alignToFlex(row ? vAlignContent : hAlignContent)

    let flexDirection = 'column'
    if (row) {
      flexDirection = reverse ? 'row-reverse' : 'row'
    } else if (reverse) {
      flexDirection = 'column-reverse'
    }

    //col and wrap should be default. row, no wrap, reverse should be opt in
    return (
      <Flex
        ref={ref}
        flexDirection={flexDirection}
        justifyContent={justify}
        alignItems={align}
        flexGrow={getGrow(grow)}
        flexShrink={getShrink(shrink, basis)}
        flexBasis={getBasis(basis)}
        {...rest}
      >
        {spacing
          ? processChildrenForSpacing(
              children,
              spacing,
              row,
              shouldWrapChildren
            )
          : children}
      </Flex>
    )
  }
)

const FlexContainerWrapper = forwardRef(({ center, ...rest }, ref) => {
  const newProps = {}
  if (typeof center === 'boolean' && center === true) {
    //vertically and horizontally center
    newProps.vAlignContent = 'center'
    newProps.hAlignContent = 'center'
  } else if (typeof center === 'string' && center === 'vertical') {
    newProps.vAlignContent = 'center'
  } else if (typeof center === 'string' && center === 'horizontal') {
    newProps.hAlignContent = 'center'
  }
  //if hAlignContent or vAlignContent was setm override
  return <FlexContainer ref={ref} {...newProps} {...rest} />
})

FlexContainer.defaultProps = {
  display: 'flex',
  row: false,
  inline: false,
  basis: '',
  centered: true,
  shrink: false,
  grow: false,
  spacing: false,
  shouldWrapChildren: false,
}
//TODO: define boxSizing: 'border-box'

export default styled(FlexContainerWrapper)(colorStyle)
