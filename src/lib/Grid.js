import React from 'react'
import { SimpleGrid } from '@chakra-ui/core' //TODO:add to lib
/** @jsx jsx */
import { jsx } from 'theme-ui'

const Grid = ({ templateAreas, _landscape, ...props }) => {
  console.log('type of ta', Array.isArray(templateAreas))
  if (
    templateAreas &&
    Array.isArray(templateAreas) &&
    Array.isArray(templateAreas[0])
  ) {
    //array of arrays - transform string arrays into quote wrapped strings
    templateAreas = templateAreas.map(area => `"${area.join('" "')}"`)
    console.log('templateAreas', templateAreas)
  }
  //box logic added here until we rewrite grid with our box component
  return (
    <SimpleGrid
      templateAreas={templateAreas}
      sx={{
        '@media (orientation: landscape)': { ..._landscape },
      }}
      {...props}
    />
  )
}

// export default SimpleGrid //TODO:fork using our Box
export default Grid //TODO:fork using our Box
