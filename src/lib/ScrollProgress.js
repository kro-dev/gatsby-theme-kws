import React, { useState } from 'react'
/** @jsx jsx */
import { jsx, useThemeUI } from 'theme-ui'
import { alpha } from 'theme/utils'
import { useViewportScroll } from 'framer-motion'
//TODO prop to switch between horizontal and vertical
const ScrollProgress = ({
  // scroll,
  horizontal = false,
  size = '4px',
  direction = false,
  pin = false,
  //   color = 'rgba(250, 224, 66, .8)',
  color = 'primary.500',
  opacity = 0.8,
  zIndex = 'scroll-progress',
  ...props
}) => {
  const { scrollYProgress: scroll } = useViewportScroll()

  direction = direction ? direction : horizontal ? 'right' : 'bottom'
  pin = pin ? pin : horizontal ? 'top' : 'right'
  const position = {}
  if (pin === 'right') {
    position.right = 0
  } else if (pin === 'bottom') {
    position.bottom = 0
  }

  const { theme } = useThemeUI()
  const computedColor = alpha(color, 1 - opacity)(theme)

  //TODO:change name of scroll prop to percent
  if (typeof scroll === 'number') {
    scroll = `${scroll * 100}%`
  }
  return (
    <div
      sx={{
        position: 'fixed',
        background: `linear-gradient(to ${direction}, ${computedColor} ${scroll}, transparent 0)`,
        // backgroundColor: alpha('primary.500', 1 - opacity),
        width: horizontal ? '100%' : size,
        height: horizontal ? size : '100%',
        zIndex,
        ...position,
      }}
      {...props}
    />
  )
}

// const WindowScrollProgress = () => {
//   const { x, y } = useWindowScrollPercent()
//   return <ScrollProgress scroll={y} />
// }

// const WindowScrollProgress = () => {
//   const scroll = useScrollPercent()
//   return <ScrollProgress scroll={scroll} />
// }

export default ScrollProgress
// export default WindowScrollProgress
//
