import React from 'react'
import { Text } from 'lib'
import styled from '@emotion/styled'

const ExternalLink = styled.a({ textDecoration: 'underline' })

export default props => (
  <Text size="xs" textAlign="center" {...props}>
    To prevent spam, this site is protected by the Google reCAPTCHA tool. The
    Google{' '}
    <ExternalLink href="https://policies.google.com/privacy">
      Privacy Policy
    </ExternalLink>{' '}
    and{' '}
    <ExternalLink href="https://policies.google.com/terms">
      Terms of Service
    </ExternalLink>{' '}
    apply.
  </Text>
)
