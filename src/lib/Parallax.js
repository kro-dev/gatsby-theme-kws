import React, { useRef, useState, useLayoutEffect } from 'react'
import {
  useViewportScroll,
  useTransform,
  useSpring,
  motion,
} from 'framer-motion'
/** @jsx jsx */
import { jsx } from 'theme-ui'
import { Flex } from 'lib'
const calculateMinHeight = (height, range) => {
  return height + height * range
}
//TODO: use context instead of multiple resize event listeners
//range (factor) - distance (and direction) relative to itself it can travel (transform) (2 is twice as far)
//top/bottom offset - distance from top/bottom of element where translation (parallax) is in effect
// useTransform(
//   scrollY,
//   [elementTop + topOffset, elementTop + bottomOffset],
//   ['0%', `${range * 100}%`]
// )
//inverse will make the element start at max range offset and transform to natural location
//smaller bottom offset reduces the distance paralax is enabled
const ParallaxItem = ({
  className,
  damping = 100,
  stiffness = 100,
  topOffset = -500,
  bottomOffset = 500,
  range = 0.2,
  mass = 1, // rand(1, 3),
  horizontal = false,
  inverse = false,
  ...props
}) => {
  const { scrollY } = useViewportScroll()
  const ref = useRef()
  const [minHeight, setMinHeight] = useState('auto')
  const [elementTop, setElementTop] = useState(0)

  const springConfig = {
    damping,
    stiffness,
    mass,
  }

  useLayoutEffect(() => {
    if (!ref.current) return
    const onResize = () => {
      setElementTop(ref.current.offsetTop)
      setMinHeight(calculateMinHeight(ref.current.offsetHeight, range))
    }

    onResize()
    window.addEventListener('resize', onResize)
    return () => window.removeEventListener('resize', onResize)
  }, [ref, range])

  // const y = useSpring(
  //   useTransform(
  //     scrollY,
  //     [elementTop + topOffset, elementTop + bottomOffset],
  //     // ['0%', `${range * 100}%`]
  //     inverse ? [`${range * 100}%`, '0%'] : ['0%', `${range * 100}%`]
  //   ),
  //   springConfig
  // )
  const mv = useSpring(
    useTransform(
      scrollY,
      [elementTop + topOffset, elementTop + bottomOffset],
      // ['0%', `${range * 100}%`]
      // inverse ? [`${range * 100}%`, '0%'] : ['0%', `${range * 100}%`]
      [1, 100]
    ),
    springConfig
  )

  const xy = horizontal ? 'x' : 'y'
  // console.log(style, 'style')
  // const style = animate.map(attr => )
  //by default, transform vertical
  //if horizontal
  return (
    <Flex
      as={motion.div}
      sx_={{ minHeight }}
      ref={ref}
      initial={{ [xy]: mv.current }}
      style={{ [xy]: mv }}
      {...props}
    />
  )
  //TODO:make sure sx from parent works (spread props if not)
  // return (
  //   <motion.div
  //     sx={{ minHeight }}
  //     ref={ref}
  //     initial={{ y: y.current }}
  //     style={{ y }}
  //   >
  //     {children}
  //   </motion.div>
  // )
}
// const rand = (min = 0, max = 100) => {
//   return Math.floor(Math.random() * (+max - +min)) + +min
// }

//input = [topOffset, bottomOffset]
//output = transform/interp. ex ['0%','100%'] ['-500px', '0px']

// const defaultSpringConfig =
const useParallax = (
  ref,
  input,
  output,
  springConfig = {
    damping: 100,
    stiffness: 100,
    mass: 1,
  }
) => {
  const topOffset = input[0]
  const bottomOffset = input[1]

  const { scrollY } = useViewportScroll()
  // const [minHeight, setMinHeight] = useState('auto')
  const [elementTop, setElementTop] = useState(0)

  useLayoutEffect(() => {
    if (!ref.current) return
    const onResize = () => {
      setElementTop(ref.current.offsetTop)
      // setMinHeight(calculateMinHeight(ref.current.offsetHeight, range))
    }

    onResize()
    window.addEventListener('resize', onResize)
    return () => window.removeEventListener('resize', onResize)
  }, [ref, output])

  const mv = useSpring(
    useTransform(
      scrollY,
      [elementTop + topOffset, elementTop + bottomOffset],
      output
    ),
    springConfig
  )

  return mv
  //TODO: is there any way to pass in multiple outputs? since we can't call hooks in loops
}

ParallaxItem.useParallax = useParallax

// const useScrollEffect = (
//   input,
//   output,
//   springConfig = {
//     damping: 100,
//     stiffness: 100,
//     mass: 1,
//   }
// ) => {
//   const topOffset = input[0]
//   const bottomOffset = input[1]

//   const { scrollY } = useViewportScroll()
//   // const [minHeight, setMinHeight] = useState('auto')

//   const mv = useSpring(
//     useTransform(
//       scrollY,
//       input,
//       output
//     ),
//     springConfig
//   )

//   return mv
// }

export default ParallaxItem
