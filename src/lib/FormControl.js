import { FormControl as CFormControl, FormLabel } from '@chakra-ui/core'
/** @jsx jsx */
import { jsx } from 'theme-ui'

export const styles = {
  label: { textTransform: 'uppercase', fontSize: 'xs' },
}

const Label = ({ ...rest }) => (
  <FormLabel sx={{ variant: 'formControl.label' }} {...rest} />
)

const FormControl = ({ ...rest }) => (
  <CFormControl sx={{ variant: 'formControl.container' }} {...rest} />
)
Label.displayName = 'FormControl.Label'
FormControl.Label = Label

export default FormControl
