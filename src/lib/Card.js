import React from 'react'
//TODO:remove emotion styled from dependencies if we fully factor it out
/** @jsx jsx */
import { jsx, useThemeUI } from 'theme-ui'
import { Flex } from './'

export const styles = {
  borderRadius: 'default',
  // borderWidth: '1px',
  boxShadow: 'sm',
}

const Card = props => <Flex {...props} sx={{ variant: 'card' }} />

Card.defaultProps = {
  boxShadow: 'default',
  // borderRadius: 'default',
  bg: 'white',
  p: 4,
}

export default Card
