//https://github.com/davideperozzi/smoovy/blob/master/packages/utils/src/browser.ts
class Browser {
  static get uA() {
    return navigator.userAgent.toLowerCase()
  }

  static get pf() {
    return navigator.platform.toLowerCase()
  }

  static get safari() {
    return /^((?!chrome|android).)*safari/.test(this.uA)
  }

  //   static get safariVersion() {
  //     return +(this.uA.match(/version\/[\d\.]+.*safari/) || ['-1'])[0]
  //       .replace(/^version\//, '')
  //       .replace(/ safari$/, '')
  //   }

  static get firefox() {
    return this.uA.indexOf('firefox') > -1
  }

  static get chrome() {
    return /chrome/.test(this.uA)
  }

  static get ie() {
    return /msie|trident/.test(this.uA)
  }

  static get ieMobile() {
    return /iemobile/.test(this.uA)
  }

  static get webkit() {
    return /webkit/.test(this.uA)
  }

  static get operaMini() {
    return /opera mini/.test(this.uA)
  }

  static get edge() {
    return /edge\/\d./.test(this.uA)
  }

  static get ios() {
    return /ip(hone|[ao]d)/.test(this.uA)
  }

  static get mac() {
    return this.pf.indexOf('mac') > -1
  }

  static get windows() {
    return this.pf.indexOf('win') > -1
  }

  static get android() {
    return /android/.test(this.uA)
  }

  static get androidMobile() {
    return /android.*mobile/.test(this.uA)
  }

  static get blackberry() {
    return /blackberry/.test(this.uA)
  }

  static get mobile() {
    return (
      this.ieMobile ||
      this.blackberry ||
      this.androidMobile ||
      this.ios ||
      this.operaMini
    )
  }

  static get mouseWheelEvent() {
    return 'onmousewheel' in document
  }

  static get wheelEvent() {
    return 'onwheel' in document
  }

  static get keydownEvent() {
    return 'onkeydown' in document
  }

  static get touchDevice() {
    return 'ontouchstart' in window
  }

  static get mutationObserver() {
    return 'MutationObserver' in window
  }

  static get client() {
    return (
      typeof window !== 'undefined' && typeof window.document !== 'undefined'
    )
  }
}

export default Browser
