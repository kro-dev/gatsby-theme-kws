//https://github.com/streamich/react-use/blob/master/src/useScroll.ts
import { useEffect, useRef, useState } from 'react'
import { motionValue } from 'framer-motion'
import { useScrollPosition, useResizeObserver, useRafState } from 'hooks'

const scrollX = motionValue(0)
const scrollY = motionValue(0)
const scrollXProgress = motionValue(0)
const scrollYProgress = motionValue(0)

//TODO: try removing resize observer before trying to use a single resize observer via context
//TODO;try chaining transforms on scroll effects
const useScroll = ref => {
  if (process.env.NODE_ENV === 'development') {
    if (typeof ref !== 'object' || typeof ref.current === 'undefined') {
      console.error('`useScroll` expects a single ref argument.')
    }
  }

  // const [state, setState] = useRafState({
  //   scrollX: 0,
  //   scrollY: 0,
  //   scrollXProgress: 0,
  //   scrollYProgress: 0,
  // })

  // const scrollX = motionValue(0)
  // const scrollY = motionValue(0)
  // const scrollXProgress = motionValue(0)
  // const scrollYProgress = motionValue(0)

  // const [state, setState] = useRafState({
  //   scrollX,
  //   scrollY,
  //   scrollXProgress,
  //   scrollYProgress,
  // })

  // const [state, setState] = useState({
  //   scrollX: motionValue(0),
  //   scrollY: motionValue(0),
  //   scrollXProgress: motionValue(0),
  //   scrollYProgress: motionValue(0),
  // })

  const { width, height } = useResizeObserver({ ref })
  // useEffect(() => {
  //   if (ref.current !== null) {
  //     setState({
  //       ...state,
  //       scrollWidth: ref.current.scrollWidth,
  //       scrollHeight: ref.current.scrollHeight,
  //     })
  //   }
  // }, [width, height, ref])

  // const width = 0
  // const height = 0

  // useResizeObserver({
  //   ref,
  //   onResize: ({ width, height }) => {
  //     // width = width
  //     console.log('here', width, height)
  //   },
  // })

  const scrollHeight = ref.current ? ref.current.scrollHeight : 0
  const scrollWidth = ref.current ? ref.current.scrollWidth : 0

  const { x, y } = useScrollPosition(ref)

  useEffect(() => {
    scrollX.set(x)
    scrollY.set(y)

    scrollXProgress.set(x / (scrollWidth - width))
    scrollYProgress.set(y / (scrollHeight - height))

    // const scrollXProgress = x / (scrollWidth - width)
    // const scrollYProgress = y / (scrollHeight - height)

    // setState({
    //   ...state,
    //   // scrollX: x,
    //   // scrollY: y,
    //   scrollX,
    //   scrollY,
    //   scrollXProgress,
    //   scrollYProgress,
    // })
  }, [ref, width, height, x, y])

  // return state
  return {
    scrollX,
    scrollY,
    scrollXProgress,
    scrollYProgress,
  }
}

export default useScroll
