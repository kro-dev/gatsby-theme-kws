import { useState, useEffect, useCallback } from 'react'
import { useGesture } from 'react-use-gesture'
import { motionValue } from 'framer-motion'
import { useResizeObserver } from 'hooks'
import { Browser } from 'utils'

//TODO:what happens if we define these in the hook?
const scrollX = motionValue(0)
const scrollY = motionValue(0)
const scrollXProgress = motionValue(0)
const scrollYProgress = motionValue(0)

const BROSWER_FIREFOX = Browser.firefox //is this needed? might not be actually checking everytime

const clamp = (val, min, max) => Math.min(Math.max(min, val), max)
//TODO: handle ref/domtarget/ if ref provided, pass to useFEsture options, don't use ...bind() on component
const useVirtualScroll = (
  ref,
  { onScroll, width, height, deltaMultiplier = 1, multiplierFirefox = 25 }
) =>
  //   { wheel = true, touch = true, arrows = true }
  {
    if (process.env.NODE_ENV === 'development') {
      //TODO:create util function to reuse
      if (typeof ref !== 'object' || typeof ref.current === 'undefined') {
        console.error('`useVirtualScroll` expects a single ref argument.')
      }
    }

    // const { width, height } = useResizeObserver({ ref }) //TODO:consider standardizing all hooks taht use ref to
    //TODO:can we programtatically add resize observer if no ref provided?

    //match params. eg, ref object, then options (ref, {...options)} =>

    //    const handleTouchMove = (event) => {
    //      if (down) {
    //        event.preventDefault()

    //        const delta = { x: 0, y: 0 }
    //        const touch = getTouch(event)

    //        delta.x = (touch.pageX - startPos.x) * cfg.deltaMultiplier
    //        delta.y = (touch.pageY - startPos.y) * cfg.deltaMultiplier

    //        const deltaTime = Ticker.now() - lastMove

    //        velocity.x = (startPos.x - touch.pageX) / deltaTime
    //        velocity.y = (startPos.y - touch.pageY) / deltaTime
    //        velocity.x *= -1 * cfg.velocityMultiplier
    //        velocity.y *= -1 * cfg.velocityMultiplier

    //        startPos.x = touch.pageX
    //        startPos.y = touch.pageY
    //        lastMove = Ticker.now()

    //        scroller.emit(ScrollerEvent.DELTA, delta)
    //      }
    //    }

    // const [state, setState] = useState({ x: 0, y: 0 })

    //   const onScroll = state => console.log(state)
    const handleWheel = useCallback(
      ({ delta }) => {
        // console.log(deltaMode, 'deltaMode')
        const [dx, dy] = delta

        let computedDx = dx * deltaMultiplier
        let computedDy = dy * deltaMultiplier

        // if (Browser.firefox && event.deltaMode === 1) {
        if (BROSWER_FIREFOX) {
          computedDx *= multiplierFirefox
          computedDy *= multiplierFirefox
        }
        const x = clamp(scrollX.get() + computedDx, 0, width)
        const y = clamp(scrollY.get() + computedDy, 0, height)

        scrollX.set(x)
        scrollY.set(y)

        scrollXProgress.set(x / width)
        scrollYProgress.set(y / height)

        //   scrollXProgress.set(x / (scrollWidth - width))
        // scrollYProgress.set(y / (scrollHeight - height))

        //   scrollX.set(clamp(scrollX.get() + dx, 0, width))
        //   scrollY.set(clamp(scrollY.get() + dy, 0, height))
        //   onScroll(scrollY.get())
        //   console.log(scrollY.get())
      },
      [deltaMultiplier, width, height, multiplierFirefox]
    )

    const handleDrag = ({ delta }) => {
      const [dx, dy] = delta

      const x = clamp(scrollX.get() - dx * deltaMultiplier, 0, width)
      const y = clamp(scrollY.get() - dy * deltaMultiplier, 0, height)

      scrollX.set(x)
      scrollY.set(y)

      scrollXProgress.set(x / width)
      scrollYProgress.set(y / height)
    }

    const options = { axis: 'y' }

    const bind = useGesture(
      { onWheel: handleWheel, onDrag: handleDrag },
      { domTarget: ref, drag: options, wheel: options }
    )
    return [{ scrollY, scrollYProgress }, bind]
  }

export default useVirtualScroll
