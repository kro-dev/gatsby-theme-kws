// //TODO: switch to @theme-ui/match-media once it is pushed out
// //https://github.com/dburles/theme-ui/blob/match-media/packages/match-media/src/index.js
// import { useState, useEffect, useCallback } from 'react'
// import { useThemeUI } from 'theme-ui'
// // //import { canUseDOM } from "exenv";
// // //TODO:switch from custom method to canusedom
// // // Shared with @styled-system/css
// const defaultBreakpoints = [40, 52, 64].map(n => n + 'em')

// export const useBreakpointIndex = (fallbackIndex = 0) => {
//   const {
//     theme: { breakpoints = defaultBreakpoints },
//   } = useThemeUI()

//   const getIndex = useCallback(
//     () =>
//       breakpoints.filter(breakpoint =>
//         typeof window !== 'undefined'
//           ? window.matchMedia(`screen and (min-width: ${breakpoint})`).matches
//           : fallbackIndex
//       ).length,
//     [breakpoints]
//   )

//   const [value, setValue] = useState(getIndex)

//   useEffect(() => {
//     const onResize = () => {
//       console.log('useResponsiveValue-onResize-start')
//       const newValue = getIndex()
//       console.log('useResponsiveValue-onResize-newValue', newValue)

//       if (value !== newValue) {
//         setValue(newValue)
//       }
//     }
//     window.addEventListener('resize', onResize)
//     return () => window.removeEventListener('resize', onResize)
//   }, [breakpoints, getIndex, value])

//   return value
// }

// export const useResponsiveValue = (values, fallbackIndex) => {
//   const { theme } = useThemeUI()
//   const array = typeof values === 'function' ? values(theme) : values
//   const index = useBreakpointIndex(fallbackIndex)
//   console.log('useResponsiveValue-onResize-index', index)
//   console.log(
//     'useResponsiveValue-onResize-retVal',
//     array[index >= array.length ? array.length - 1 : index]
//   )
//   return array[index >= array.length ? array.length - 1 : index]
// }

// export default useResponsiveValue

import { useResponsiveValue, useBreakpointIndex } from '@theme-ui/match-media'
export default useResponsiveValue
