import { useRafState } from 'react-use'

// const Demo = () => {
//   const [state, setState] = useRafState({
//     width: 0,
//     height: 0,
//   })

//   useMount(() => {
//     const onResize = () => {
//       setState({
//         width: window.clientWidth,
//         height: window.height,
//       })
//     }

//     window.addEventListener('resize', onResize)

//     return () => {
//       window.removeEventListener('resize', onResize)
//     }
//   })

//   return <pre>{JSON.stringify(state, null, 2)}</pre>
// }

export default useRafState
