import React from 'react'

//consider instead of using {...bind}, pass in a ref and add listener that way
const useHover = () => {
  const [isHovering, setHover] = React.useState(false)
  return {
    isHovering,
    bind: {
      onMouseEnter: () => setHover(true),
      onMouseLeave: () => setHover(false),
    },
  }
}

export default useHover
