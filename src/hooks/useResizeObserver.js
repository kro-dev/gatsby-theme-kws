// import useResizeObserver from 'use-resize-observer'
import useResizeObserver from 'use-resize-observer/polyfilled'

// const [ref, width, height] = useResizeObserver();
export default useResizeObserver
