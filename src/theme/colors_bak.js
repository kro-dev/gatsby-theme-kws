const namedColors = {
  navy: '#001f3f',
  blue: '#0074D9',
  aqua: '#7FDBFF',
  teal: '#39CCCC',
  olive: '#3D9970',
  green: '#2ECC40',
  lime: '#01FF70',
  yellow: '#FFDC00',
  orange: '#FF851B',
  red: '#FF4136',
  maroon: '#85144b',
  fuchsia: '#F012BE',
  purple: '#B10DC9',
  black: '#111111',
  grey: '#AAAAAA',
  silver: '#DDDDDD',
  white: '#FFFFFF',
}

//maybe color array
const greys = {
  charcoal: '#323232',
  steel: '#666666',
  alto: '#DADADA',
  porcelain: '#F4F5F6',
  dusty: '#999999',
  mineShaft: '#222222',
}

const whites = {
  ghost: '#F8F8FF',
  snow: '#FFFAFA',
  honeydew: '#F0FFF0',
  alice: '#F0F8FF',
  smoke: '#F5F5F5',
}

const themeColors = {
  primary: namedColors.blue,
  secondary: namedColors.teal,
}

const ui = {
  link: namedColors.red,
  linkHover: namedColors.maroon,
  info: namedColors.blue,
  infoBackground: namedColors.lightBlue,
  error: namedColors.orange,
  errorBackground: namedColors.lightOrange,
  success: namedColors.green,
  successBackground: namedColors.lightGreen,
  // modalNavBackground: themeColors.primary,
  text: namedColors.black,
  heading: namedColors.black,
  background: namedColors.white,
  modalNav: { bg: namedColors.white },
}
//shim for now, remove ui key in favor for plain
const colors = { ...namedColors, ...themeColors, ui, ...ui, greys, whites }

//shim for now
colors.brand = {
  primary: themeColors.primary,
  secondary: themeColors.secondary,
}
export default colors
