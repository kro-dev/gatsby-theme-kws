import theme from './theme'
export default theme
export { theme }
export { default as colors } from './colors'
export { default as variants } from './variants'
export { default as config } from './config'
