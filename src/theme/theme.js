import colors from './colors'
import sizes, { baseSizes } from './sizes'
import typography from './typography'
import variants from './variants'
import icons from './icons'
import config from './config'

const space = baseSizes

const shadows = {
  sm: '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
  md: '0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
  lg: '0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)',
  xl:
    '0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04)',
  '2xl': '0 25px 50px -12px rgba(0, 0, 0, 0.25)',
  outline: '0 0 0 3px rgba(66, 153, 225, 0.6)',
  inner: 'inset 0 2px 4px 0 rgba(0,0,0,0.06)',
  none: 'none',
}

// const breakpoints = ['26em', '46em', '80em', '120em']
const breakpoints = ['52em', '86em', '136em']
// aliases
breakpoints.sm = breakpoints[0]
breakpoints.md = breakpoints[1]
breakpoints.lg = breakpoints[2]

const zIndices = {
  hide: -1,
  auto: 'auto',
  base: 0,
  docked: 10,
  dropdown: 1000,
  footer: 100,
  sticky: 1100,
  banner: 1200,
  overlay: 1300,
  modal: 1400,
  popover: 1500,
  skipLink: 1600,
  toast: 1700,
  tooltip: 1800,
  //end chakra
  'layout-header': 4000,
  'transition-overlay': 3000,
  'scroll-progress': 2500,
}

const radii = {
  none: '0',
  xs: '0.125rem',
  sm: '0.25rem',
  md: '0.5rem',
  lg: '1rem',
  xl: '2rem',
  full: '9999px', //50%?
}
radii.default = radii.none

const opacity = {
  '0': '0',
  '20%': '0.2',
  '40%': '0.4',
  '60%': '0.6',
  '80%': '0.8',
  '100%': '1',
}

const borders = {
  none: 0,
  '1px': '1px solid',
  '2px': '2px solid',
  '3px': '2px solid',
  '4px': '4px solid',
}

borders.button = {
  default: borders.none,
  outline: {
    default: borders['2px'],
    hover: borders['1px'],
  },
}

// const borderWidths = {
//   xl: '2rem',
//   lg: '1rem',
//   md: '0.5rem',
//   sm: '0.25rem',
//   xs: '0.125rem',
//   '2xs': '0.0625rem',
//   none: 0,
// }

const transitions = {
  quick: '222ms cubic-bezier(0.060, 0.975, 0.195, 0.985)',
  base: '220ms ease-in-out',
  slow: '500ms ease-in-out',
}
transitions.default = transitions.base

const theme = {
  breakpoints,
  zIndices,
  radii,
  opacity,
  borders,
  // borderWidths,
  colors,
  ...typography,
  sizes,
  shadows,
  space,
  transitions,
  // icons,
  ...variants,
}

theme.config = config
theme.icons = icons
export default theme
