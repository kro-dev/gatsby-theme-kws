//this is for use in @charkaui css reset (which also applies some global styling like body bg, etc)
const chakraConfig = theme => ({
  light: {
    color: theme.colors.text,
    bg: theme.colors.background,
    borderColor: theme.colors.gray[200],
    placeholderColor: theme.colors.gray[400],
  },
  dark: {
    color: theme.colors.whiteAlpha[900],
    bg: theme.colors.gray[800],
    borderColor: theme.colors.whiteAlpha[300],
    placeholderColor: theme.colors.whiteAlpha[400],
  },
})
export default { modalNavOnly: true, chakraConfig }
