import React from 'react'
import { TransitionProvider } from 'lib/Layout/TransitionContext'

// just export a WrapRoot function so we dont have to write this twice in
//gatsby-browser and gatsby-ssr
export const wrapRootElement = ({ element }) => (
  <TransitionProvider>{element}</TransitionProvider>
)
